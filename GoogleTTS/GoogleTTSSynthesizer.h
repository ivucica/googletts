//
//  GoogleTTSSynthesizer.h
//  GoogleTTS
//
//  Created by Ivan Vučica on 4.8.2012..
//  Copyright (c) 2012. Ivan Vučica. All rights reserved.
//

#import <Foundation/Foundation.h>

#include <CoreAudio/CoreAudio.h>
#include <AudioToolbox/AudioToolbox.h>
#include <dispatch/dispatch.h>
@class AudioOutput;
@class AVAudioPlayer;
#import <AVFoundation/AVFoundation.h>

@interface GoogleTTSSynthesizer : NSObject<AVAudioPlayerDelegate>
{
	//
	// Synthesizer state (applicable to most synthesizers)
	//
	int					synthState;
	float				speechRate;
	float				pitchBase;
	float				volume;
	CFStringRef			openDelim;
	CFStringRef			closeDelim;
	SRefCon				clientRefCon;
	CFStringRef			textBeingSpoken;
	
	//
	// Audio output state (applicable to most synthesizers)
	//
	AudioOutput *		audioOutput;
	ExtAudioFileRef		audioFileRef;	    // Audio file to save to
	bool				audioFileOwned;		// Did we open it?
	AudioDeviceID		audioDevice;		// Audio device to play to	
	//
	// Callbacks
	//
	SpeechTextDoneProcPtr	textDoneCallback;	// Callback to call when we no longer need the input text
	SpeechDoneProcPtr		speechDoneCallback;	// Callback to call when we're done
	SpeechSyncProcPtr		syncCallback;		// Callback to call for sync embedded command
	SpeechWordCFProcPtr		wordCallback;		// Callback to call for each word 
	//
	// We work through a dispatch queue. It's the modern thing to do
	//
	dispatch_queue_t	queue;
	dispatch_source_t	generateSamples;
    
    /////////////
    NSMutableString * textToSpeak;
    AVAudioPlayer * player;
    NSString * voiceName;
}

@property (nonatomic, retain) AVAudioPlayer *player;
@property (nonatomic, retain) NSString * voiceName;

- (id)init;
- (void)close;	
- (long)useVoice:(VoiceSpec *)voice withBundle:(CFBundleRef)inVoiceSpecBundle;
- (long)startSpeaking:(CFStringRef)text 
	  noEndingProsody:(BOOL)noEndingProsody noInterrupt:(BOOL)noInterrupt 
			preflight:(BOOL)preflight;
- (long)copyPhonemes:(CFStringRef)text result:(CFStringRef *)phonemes;
- (long)stopSpeakingAt:(unsigned long)whereToStop;
- (long)pauseSpeakingAt:(unsigned long)whereToStop;
- (long)continueSpeaking;
- (CFStringEncoding)stringEncodingForBuffer;

+ (long)willUnloadBundle;

// Private
- (void)createSoundChannel:(BOOL)forAudioUnit;
- (void)disposeSoundChannel;


@end

/*
 Private: state
 */

enum SynthState {
	kSynthStopped,	// Ready for next speech call
	kSynthRunning,	// Generating audio
	kSynthPaused,	// More tokens available, but paused
	kSynthStopping,	// Stop after generating next batch of samples
	kSynthPausing	// Pause after generating next batch of samples
};



//
//  GoogleTTSSynthesizer.m
//  GoogleTTS
//
//  Created by Ivan Vučica on 4.8.2012..
//  Copyright (c) 2012. Ivan Vučica. All rights reserved.
//

#import "GoogleTTSSynthesizer.h"
#import <AVFoundation/AVFoundation.h>

@implementation GoogleTTSSynthesizer
@synthesize player;
@synthesize voiceName;

+ (long)willUnloadBundle
{
	return 0; /* We retain no resources that would block unloading us */
}

- (id)init
{
    self = [super init];
    if(!self)
        return nil;
    
    queue = dispatch_queue_create("GoogleTTSSynthesizer", 0);
    
    return self;
}

- (void)dealloc
{
    self.player = nil;
    self.voiceName = nil;
    [super dealloc];
}

- (void)close
{
	if (generateSamples)
		dispatch_source_cancel(generateSamples);
	dispatch_release(queue);
	[self release];
}

- (long)useVoice:(VoiceSpec *)voice withBundle:(CFBundleRef)inVoiceSpecBundle
{
	/*
	 * Set up voice specific information. Normally, we would use a considerable amount of voice
	 * specific data. 
     * Google offers no parameters, though.
	 */
	VoiceDescription desc;
	if (!GetVoiceDescription(voice, &desc, sizeof(desc)))
    {
        /*
		switch (desc.gender) {
            case kMale:
                speechRate	= 20.0f;
                pitchBase	= 300.0f;
                break;
            case kFemale:
                speechRate	= 25.0f;
                pitchBase	= 440.0f;
                break;
            case kNeuter:
                speechRate	= 30.0f;
                pitchBase	= 360.0f;
                break;
		}
         */
        char cVoiceName[64];
        memcpy(cVoiceName, desc.name+1, desc.name[0]);
        cVoiceName[desc.name[0]] = 0;
        
        self.voiceName = [NSString stringWithUTF8String:cVoiceName];
    }
	return noErr;
}

- (BOOL)isActive
{
	__block BOOL result;
	
	dispatch_sync(queue, ^{
        result = !(synthState == kSynthStopped || synthState == kSynthPaused);
    });
	
	return result;
}


- (void)encodeText:(CFStringRef)text
{
#if 0
	[tokens clear];
	if (wordCallback)
		[tokens wantWordCallbacks:@selector(wordCallback:)];
#else
    [textToSpeak release];
    textToSpeak = [NSMutableString new];
#endif
    
	/*
	 * Morse code is case insensitive, and so are our embedded commands.
	 * Most synthesizers require considerably more subtlety than this.
	 */
	CFMutableStringRef s = CFStringCreateMutableCopy(NULL, 0, text);
	CFStringFold(s, kCFCompareCaseInsensitive, NULL);
	text = s;
    
	float	curRate	= speechRate;
	float	curPitch= pitchBase;
	float	curVol	= volume;
    
	CFRange remainingText = CFRangeMake(0, CFStringGetLength(text));
	while (openDelim) {
		CFRange delim;
		if (!CFStringFindWithOptions(text, openDelim, remainingText, 0, &delim))
			break;
        
		/* Encode text before opening delimiter */
		CFRange prefix = CFRangeMake(remainingText.location,
									 delim.location-remainingText.location);
#if 0
        // We don't use tokens
		[tokens encodeText:text range:prefix];
		[tokens encodeWordBreak];
#else
        [textToSpeak appendString:[(NSString*)text substringWithRange:NSMakeRange(prefix.location, prefix.length)]];
#endif
        
		/* Process embedded commands */
		CFStringRef newOpenDelim	= (CFStringRef)CFRetain(openDelim);
		CFStringRef newCloseDelim   = (CFStringRef)CFRetain(closeDelim);
		CFIndex		embedded		= delim.location+delim.length;
		CFIndex     endEmbedded     = remainingText.location+remainingText.length;
        
		delim.length = remainingText.length-(embedded-remainingText.location);
		if (CFStringFindWithOptions(text, closeDelim, delim, 0, &delim)) {
			endEmbedded 			= delim.location;
			remainingText.length 	= (remainingText.location+remainingText.length)-(endEmbedded+delim.length);
			remainingText.location	= endEmbedded+delim.length;
		} else {
			remainingText.length	= 0;
			remainingText.location	= endEmbedded;
		}
        
		/* We should be reporting errors if we encounter any, but we don't */
#define FETCH_NEXT_CHAR if (embedded < endEmbedded) ch = CFStringGetCharacterAtIndex(text, embedded++); else ch = ' '
#define SKIP_SPACES     while (isspace(ch) && embedded<endEmbedded) FETCH_NEXT_CHAR
#define ERROR			goto skipToNextCommand
        
		while (embedded < endEmbedded) {
			UniChar ch;
			FETCH_NEXT_CHAR;
			SKIP_SPACES;
			char 	selector[5] = {0,0,0,0,0};
			int  	selIx       = 0;
			SEL		paramSel;
			float * curParam;
			char	relative;
			char    argument[32];
			int		argIx;
			float 	value;
			while (selIx < 4 && isalpha(ch)) {
				selector[selIx++] = ch;
				FETCH_NEXT_CHAR;
			}
			/*
			 * We only handle a small subset of the embedded commands we're
			 * supposed to handle. You probably get the idea, though.
			 */
			if (!strcmp(selector, "cmnt")) {
				break; /* Comment, skip rest of embedded command */
			} else if (!strcmp(selector, "dlim")) {
				/*
				 * Change embedded command delimiters. The change takes place
				 * AFTER the current block of embedded commands.
				 */
				UniChar odelim[2] = {0,0};
				UniChar cdelim[2] = {0,0};
				SKIP_SPACES;
				if (!isspace(ch) && ch != ';') {
					odelim[0] = ch;
					FETCH_NEXT_CHAR;
					if (!isspace(ch) && ch != ';') {
						odelim[1] = ch;
						FETCH_NEXT_CHAR;
					}
					SKIP_SPACES;
				}
				if (!isspace(ch) && ch != ';') {
					cdelim[0] = ch;
					FETCH_NEXT_CHAR;
					if (!isspace(ch) && ch != ';') {
						cdelim[1] = ch;
						FETCH_NEXT_CHAR;
					}
				}
				newOpenDelim = !odelim[0] ? NULL
                : CFStringCreateWithCharacters(NULL, odelim, 1+(odelim[1] != 0));
				newCloseDelim = !cdelim[0] ? NULL
                : CFStringCreateWithCharacters(NULL, cdelim, 1+(cdelim[1] != 0));
			} else if (!strcmp(selector, "rate")) {
				paramSel= @selector(updateSpeechRate:);
				curParam= &curRate;
			handleNumericArgument:
				SKIP_SPACES;
				if (ch == '+' || ch == '-') {
					relative = ch;
					FETCH_NEXT_CHAR;
				} else
					relative = 0;
				SKIP_SPACES;
				for (argIx = 0; isdigit(ch) || ch == '.'; ++argIx) {
					argument[argIx] = ch;
					FETCH_NEXT_CHAR;
				}
				argument[argIx] = 0;
				if (!argIx)
					ERROR;
				value = atof(argument);
				/* TODO: Parameters need range check! */
				switch (relative) {
                    case '+':
                        *curParam += value;
                        break;
                    case '-':
                        *curParam -= value;
                        break;
                    default:
                        *curParam = value;
                        break;
				}
#if 0
                // No support for rate change
                [tokens encodeFloatCallback:paramSel value:*curParam];
#endif
			} else if (!strcmp(selector, "pbas")) {
				paramSel= @selector(updatePitchBase:);
				curParam= &curPitch;
				goto handleNumericArgument;
			} else if (!strcmp(selector, "volm")) {
				paramSel= @selector(updateVolume:);
				curParam= &curVol;
				goto handleNumericArgument;
			} else if (!strcmp(selector, "sync")) {
				/* Sync accepts a wide range of formats */
				uint32_t arg = 0;
				SKIP_SPACES;
				if (ch == '0') {
					FETCH_NEXT_CHAR;
					if (ch == 'x') {
					hexArg:
						FETCH_NEXT_CHAR;
						while (isxdigit(ch)) {
							arg	= arg*16 + ch - (isdigit(ch) ? '0' : 'a');
							FETCH_NEXT_CHAR;
						}
					} else {
						/* Initial 0 can be ignored */
					decimalArg:
						while (isdigit(ch)) {
							arg = arg*10 + ch-'0';
							FETCH_NEXT_CHAR;
						}
					}
				} else if (ch == '$') {
					goto hexArg;
				} else if (isdigit(ch)) {
					goto decimalArg;
				} else if (ch == '\'' || ch == '"') {
					UniChar quote = ch;
					FETCH_NEXT_CHAR;
					while (ch != quote && !(arg & 0xFF000000)) {
						arg	= (arg << 8) | (ch & 0xFF);
						FETCH_NEXT_CHAR;
					}
				} else {
					arg = ch << 24;
					FETCH_NEXT_CHAR;
					arg |= (ch & 0xFF) << 16;
					FETCH_NEXT_CHAR;
					arg |= (ch & 0xFF) << 8;
					FETCH_NEXT_CHAR;
					arg |= (ch & 0xFF);
					FETCH_NEXT_CHAR;
				}
#if 0
                // Unfortunately, we have no idea when is each phoneme played
                // in Google's TTS.
				[tokens encodeSyncCallback:@selector(syncCallback:) value:arg];
#endif
			} else {   /* Unknown selector */
				ERROR;
			}
		skipToNextCommand:
			while (embedded < endEmbedded && isspace(ch))
				FETCH_NEXT_CHAR;
			if (embedded == endEmbedded)
				break;
			else if (ch != ';') {
				FETCH_NEXT_CHAR;
				ERROR;
			}
		}
        
		if (openDelim)
			CFRelease(openDelim);
		openDelim	= newOpenDelim;
		if (closeDelim)
			CFRelease(closeDelim);
		closeDelim  = newCloseDelim;
	}
	if (remainingText.length)
#if 0
		[tokens encodeText:text range:remainingText];
#else
        [textToSpeak appendString:[(NSString*)text substringWithRange:NSMakeRange(remainingText.location, remainingText.length)]];
#endif
    
	CFRelease(s);
}


- (long)startSpeaking:(CFStringRef)text
	  noEndingProsody:(BOOL)noEndingProsody noInterrupt:(BOOL)noInterrupt
			preflight:(BOOL)preflight
{
	/*
	 * Test for currently active speech
	 */
	if ([self isActive]) {
		if (noInterrupt) {
			return synthNotReady;
		} else {
			[self stopSpeakingAt:kImmediate];
			while ([self isActive])
				usleep(5000);	// Test again in 5ms
		}
    }
    
	synthState	= kSynthStopped;
#if 0
    // We can't pause at anything except 'current time'
    pauseToken	= kMorseNone;
#endif
    
	if (!text || !CFStringGetLength(text))
		return noErr;
	
	textBeingSpoken = CFRetain(text);
	[self encodeText:text];
	[self createSoundChannel:NO];
#if 0
	if (preflight)
		synthState = kSynthPaused;
	else
		[self startSampleGeneration];
#else
    if (!preflight)
        [player play];
#endif
	return noErr;
}

- (long)stopSpeakingAt:(unsigned long)whereToStop
{
	dispatch_sync(queue, ^{
#if 0
		switch (synthState) {
            case kSynthStopping:
            case kSynthStopped:
                break;
            case kSynthPaused:
                [tokens clear];
                synthState	= kSynthStopped;
                break;
            case kSynthPausing:
                synthState = kSynthStopping;
                break;
            case kSynthRunning:
                switch (whereToStop) {
                    case kEndOfWord:
                        [tokens trimTokens:kMorseWordGap];
                        break;
                    case kEndOfSentence:
                        [tokens trimTokens:kMorseSentenceGap];
                        break;
                    default:
                        if (audioOutput) {
                            synthState = kSynthStopping;
                            [audioOutput stopAudio];
                            [tokens clear];
                            unitsLeftInToken = 0;
                        } else {
                            synthState = kSynthStopped;
                        }
                } 
                break;
		}
#else
        // sadly, we can only stop immediately.
        [self.player stop];
#endif
	});
	
	return noErr;
}

- (long)pauseSpeakingAt:(unsigned long)whereToStop
{
	dispatch_sync(queue, ^{
#if 0
		switch (synthState) {
            case kSynthPausing:
            case kSynthPaused:
            case kSynthStopping:
            case kSynthStopped:
                break;
            case kSynthRunning:
                switch (whereToStop) {
                    case kEndOfWord:
                        pauseToken = kMorseWordGap;
                        break;
                    case kEndOfSentence:
                        if (!pauseToken)
                            pauseToken = kMorseSentenceGap;
                        break;
                    default:
                        pauseToken = kMorseNone;
                        synthState = kSynthPausing;
                        [audioOutput stopAudio];
                }
                break;
		}
#else
        // sadly, we can only stop immediately.
        [self.player pause];
#endif
	});
	
	return noErr;
}

- (long)continueSpeaking
{
#if 0
	switch (synthState) {
        case kSynthPausing:
        case kSynthPaused:
            [tokens skipGaps];
            [self startSampleGeneration];
            break;
        default:
            break;
	}
#else
    [self.player play];
#endif
    
	return noErr;
}

- (long)copyPhonemes:(CFStringRef)text result:(CFStringRef *)phonemes
{
    // This synthesizer doesn't really distinguish between a concept
    // of phonemes and text (since that's done server side).
    *phonemes = (CFStringRef)[(NSString*)text copy];
    return 0;
}

- (CFStringEncoding)stringEncodingForBuffer
{
	return NSUTF8StringEncoding; //stringEncodingForBuffer;
}

- (void)updateSpeechRate:(NSValue *)update
{
#if 0
	speechRate = ((GoogleTTSCallback *)[update pointerValue])->arg.f;
    // No support for speech rate
#endif
}

- (void)updatePitchBase:(NSValue *)update
{
#if 0
	pitchBase = ((GoogleTTSCallback *)[update pointerValue])->arg.f;
    // No support for pitch base
#endif
}

- (void)updateVolume:(NSValue *)update
{
#if 0
	volume = ((GoogleTTSCallback *)[update pointerValue])->arg.f;
    // We might support volume... but we have no support for callbacks,
    // and we ignore embedded commands.
#endif
}


- (void)wordCallback:(NSValue *)arg
{
#if 0
	if (wordCallback) {
		CFRange r = ((GoogleTTSCallback *)[arg pointerValue])->arg.r;
		wordCallback((SpeechChannel)self, clientRefCon, textBeingSpoken, r);
	}
    // Again, we don't support callbacks
#endif
}

- (void)syncCallback:(NSValue *)arg
{
#if 0
	if (syncCallback)
		syncCallback((SpeechChannel)self, clientRefCon, ((GoogleTTSCallback *)[arg pointerValue])->arg.u);
    // No support for callbacks...
#endif
}



- (void)createSoundChannel:(BOOL)forAudioUnit
{
	dispatch_sync(queue, ^{
#if 0
        if (!audioOutput)
            if (audioFileRef == (ExtAudioFileRef)-1)
                audioOutput = [AudioOutput createIgnoreAudio];
            else if (audioFileRef)
                audioOutput = [AudioOutput createFileAudio:audioFileRef];
            else
                audioOutput = [AudioOutput createLiveAudio:forAudioUnit withDevice:audioDevice];
#else
        NSDictionary *dictionary = [[[NSDictionary alloc] initWithObjectsAndKeys:
                                     @"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/534.52.7 (KHTML, like Gecko) Version/5.1.2 Safari/534.52.7", @"UserAgent",
                                     
                                     nil] autorelease];
        [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];

        NSString * lang = @"en";
        if([self.voiceName isEqualToString:@"Miku"])
            lang = @"ja";
        
        NSLog(@"VN %@", self.voiceName);
        
        NSString * urlString = [NSString stringWithFormat:@"http://translate.google.com/translate_tts?q=%@&tl=%@&ie=UTF-8", [textToSpeak stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], lang];
        NSLog(@"url %@", urlString);
        NSURL * url = [NSURL URLWithString:urlString];
        NSData * data = [NSData dataWithContentsOfURL:url];
        if(data)
        {
            self.player = [[[AVAudioPlayer alloc] initWithData:data error:nil] autorelease];
            self.player.delegate = self;
        }
        else
        {
            self.player = [[[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle bundleForClass:[self class]] pathForResource:@"problem" ofType:@"mp3"]] error:nil] autorelease];
            self.player.delegate = self;
        }
#endif
    });
}


- (void)disposeSoundChannel
{
	dispatch_sync(queue, ^{
#if 0
        [audioOutput close];
        
        audioOutput	  	= 0;
        audioDevice	= kAudioDeviceUnknown;
        
        if (audioFileRef) {
            if (audioFileOwned)
                ExtAudioFileDispose(audioFileRef);
            audioFileRef 	= 0;
            audioFileOwned 	= NO;
        }
#else
        [player release];
        player = nil;
#endif
    });
}



/////////////
// MARK: AVAudioPlayer delegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag;
{
    if (textDoneCallback) {
        /* The text done callback used to allow clients to pass in more text,
         but that feature is deprecated. We pass a NULL pointer so clients
         will hopefully get the hint.
         */
        const void * 	nextBuf		= NULL;
        unsigned long 	byteLen		= 0;
        SInt32	   		controlFlags= 0;
        textDoneCallback((SpeechChannel)self, clientRefCon, &nextBuf, &byteLen, &controlFlags);
    }

    speechDoneCallback((SpeechChannel)self, clientRefCon);
}

@end

GoogleTTS for OS X
==================

This is an implementation of a voice synthesizer for OS X which uses
Google Translate's text to speech private API. It's based on Apple's
example code `MorseSynthesizer`. It comes with two voices, "Elizabeth"
for English and "Miku" for Japanese. (These are random names that have
nothing to do with Google's internal names for these voices, if such
names exist. In any case, their entire difference is choice of
different language from Google Translate.)

It works by requesting the synthesized MP3 file from `http://translate.google.com/translate_tts?q=INPUTTEXT&tl=LANGUAGE&ie=UTF-8` into a `NSData` object and then playing it using `AVAudioPlayer`.
It also forces the user agent to be something more sensible than
the default when `dataWithContentsOfURL:` is used - otherwise,
Google Translate's TTS refuses to work.

Unfortunately, playing via `AVAudioPlayer` is incorrect and fails 
miserably in anything but the command line utility `say`. To actually
work properly, Core Audio should be utilized to play the data from
and in-memory buffer. That's what `MorseSynthesizer` does, but
that requires fiddling with playing an MP3 using Core Audio, which
for this quick hack I didn't want to do.

To install:

    sudo xcodebuild DSTROOT=/ install

Note: this uses a private API from Google, and a barely documented
and rarely used plugin interface from Apple. I'm really not familiar
with internals of either, and Core Audio is also somewhat abstract
to me. I did some work with it previously, so I may end up playing
with playing through it, but for now my goal of having a sensible
Japanese TTS via at least command line is satisfied.

For the time being, anything written by Apple is covered by the
Apple license (as documented in a few files) and anything else is
to be considered to be under a BSD license and (c) 2012 Ivan Vučica.

Patches are welcome.

Ivan Vučica - <ivan@vucica.net>
